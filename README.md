# README

read this carefully, for feedback / questions contact florian

This is the blueprint structure for a CPU application.

If your application requires CUDA, please refer to:
https://bitbucket.org/neuronflow/docker_blueprint_kraken/src/master/

## copy paste your code

look through the repo. fill in the code at the places indicated by the comments.
pay special attention to `.env` and `.Dockerfile` and fill in your author information and application information there

## system specs / base image

Python 3.8-buster

## building docker image and saving it

follow the instructions above, please

### first build docker image

```
docker build -t your_application_name .

```

### run it for testing

```
docker run -it --rm --name your_application_name your_application_name
```

### save docker image

```
docker save your_application_name | gzip > ./docker_builds/your_application_name.tar.gz
```

## deploy

contact florian on slack to discuss deployment.
