from code.utils.utilities import helper
from shutil import copyfile

# load env
from dotenv import load_dotenv
import os

load_dotenv()
APPLICATION_NAME = os.getenv("APPLICATION_NAME")
APPLICATION_VERSION = os.getenv("APPLICATION_VERSION")

INPUTFOLDER = os.getenv("INPUTFOLDER")
OUTPUTFOLDER = os.getenv("OUTPUTFOLDER")


def logicWrapper():
    inputFile = os.path.normpath(INPUTFOLDER + "input.nii.gz")
    outputFile = os.path.normpath(OUTPUTFOLDER + "output.nii.gz")
    
    # copy paste your amazing logic here
    print("wrapper: I can feel the magic happening..it feels like a little sun rising inside me!")

    # example logic
    copyfile(inputFile, outputFile)
    helper()


def runCode():
    print("*** code execution started:",
          APPLICATION_NAME, "version:", APPLICATION_VERSION, "! ***")
    logicWrapper()
    print("*** code execution finished:",
          APPLICATION_NAME, "version:", APPLICATION_VERSION, "! ***")
